package main

import "fmt"

type Node struct {
	Name  string
	Left  *Node
	Right *Node
}

type UnbalanceBinaryTree struct {
	Root *Node
}

func enqueue(queue []*Node, element *Node) []*Node {
	queue = append(queue, element) // Simply append to enqueue.
	// fmt.Println("Enqueued:", element)
	return queue
}

func dequeue(queue []*Node) (*Node, []*Node) {
	element := queue[0] // The first element is the one to be dequeued.
	queue = queue[1:]
	// fmt.Println("Dequeued:", element)
	return element, queue
}

func printInOrder(queue []*Node) {
	for _, node := range queue {
		fmt.Printf("%s ", node.Name)
	}
}

func printReverseOrder(queue []*Node) {
	for i := len(queue) - 1; i >= 0; i-- {
		node := queue[i]
		fmt.Printf("%s ", node.Name)
	}
}

func (t *UnbalanceBinaryTree) ZigZagTraversal() {

	var current []*Node
	var children []*Node

	currentLevel := 0

	current = enqueue(current, t.Root)

	for len(current) > 0 {

		if currentLevel%2 == 0 {
			printInOrder(current)
		} else {
			printReverseOrder(current)
		}

		for len(current) > 0 {
			var node *Node
			node, current = dequeue(current)

			if node.Left != nil {
				children = enqueue(children, node.Left)
			}
			if node.Right != nil {
				children = enqueue(children, node.Right)
			}
		}

		current = children
		children = nil
		currentLevel++
	}
}

func main() {
	fmt.Println("Zig Zag Traversal")
	tree := createSampleTree()
	tree.ZigZagTraversal()
}

func createSampleTree() *UnbalanceBinaryTree {
	// Create nodes
	// Level 5
	k := &Node{
		Name: "K",
	}

	l := &Node{
		Name: "L",
	}

	m := &Node{
		Name: "M",
	}

	n := &Node{
		Name: "N",
	}

	// Level 4
	g := &Node{
		Name:  "G",
		Right: k,
	}

	h := &Node{
		Name:  "H",
		Right: l,
	}

	i := &Node{
		Name:  "I",
		Right: m,
	}

	j := &Node{
		Name:  "J",
		Right: n,
	}

	// Level 3
	d := &Node{
		Name: "D",
		Left: g,
	}

	e := &Node{
		Name: "E",
		Left: h,
	}

	f := &Node{
		Name:  "F",
		Left:  i,
		Right: j,
	}

	b := &Node{
		Name: "B",
		Left: d,
	}

	c := &Node{
		Name:  "C",
		Left:  e,
		Right: f,
	}

	a := &Node{
		Name:  "A",
		Left:  b,
		Right: c,
	}

	// Build tree
	tree := &UnbalanceBinaryTree{
		Root: a,
	}

	return tree
}
